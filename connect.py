import tornado.ioloop
import tornado.web
import tornado.template as template
from  Features import Features



class MainHandler(tornado.web.RequestHandler):
    def get(self):
        ft = Features()
        ft.create_hierarchy()
        print ft.hierarchy
        loader = template.Loader(".")
        self.write(loader.load("template.html").generate( items=ft.hierarchy, title="Hierarchy View", compiled = True))
    def post(self):
        self.set_header("Content-Type", "text/plain")
        ft = Features()
        print self.request
        if self.get_body_argument("email",default=""):
            ft.remove_user_with_email(self.get_body_argument("email"))
        if self.get_body_argument("email_up2",default=""):
            ft.update_position_with_email(self.get_body_argument("email_up2"),self.get_body_argument("position_update"))
        if self.get_body_argument("email_up1",default=""):
            ft.update_name_with_email(self.get_body_argument("email_up1"),self.get_body_argument("name_update"))
        if self.get_body_argument("email_up3",default=""):
            ft.update_manager_with_email(self.get_body_argument("email_up3"),self.get_body_argument("manager_update"))
        if self.get_body_argument("email_add",default=""):
            ft.add_user_with_email(self.get_body_argument("name"),self.get_body_argument("email_add"),self.get_body_argument("position_name"),self.get_body_argument("manager_mail"))
        
def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()