from Employee import Employee
from DBOperations import DBOperations
class Features(object):
	"""class for implementing backend Features: add, remove, update and keeping the relations unbroken"""
	def __init__(self):
		self.dbo = DBOperations("localhost", "root", "", "users")
		self.employees = self.dbo.get_all_employees()
		self.managers = self.create_managers_dictionary()
		self.general_manager = self.get_general_manager()

	def create_managers_dictionary(self):
		managers = dict()
		for emp in self.employees:
			manager_id = self.employees[emp].manager_id
			if manager_id in managers:
				managers[manager_id].append(emp)
			else:
				managers[manager_id] = [emp]
		return managers
	
	def add_user_with_email(self, name,email, position, email_manager):
	    users_with_email = dict((self.employees[item].email, item) for item in self.employees)
	    self.add_user(name, position,users_with_email[email_manager], email)	
	def add_user(self, name, position, superior, email):
	    print superior
	    if superior in self.employees:
	        emp = Employee((0, name, position, superior,email))
	        self.dbo.create_employee(emp)
	        db_row = self.dbo.get_employee(email)
	        emp = Employee(db_row)
	        self.employees[emp.empid] = emp
	        if superior in self.managers:
	            self.managers[superior].append(emp.empid)
	        else:
	            self.managers[superior]=[emp.empid]
		
	def update_position_with_email(self, email, position):
	    users_with_email = dict((self.employees[item].email, item) for item in self.employees)
	    self.update_position(users_with_email[email],position)
	
	def update_name_with_email(self, email, name):
	    users_with_email = dict((self.employees[item].email, item) for item in self.employees)
	    self.update_name(users_with_email[email],name)
	    
	def update_manager_with_email(self, email, email_manager):
	    users_with_email = dict((self.employees[item].email, item) for item in self.employees)
	    self.update_manager(users_with_email[email],users_with_email[email_manager])
	
	def update_position(self, user_id, position):
	    if not user_id == self.general_manager and not position == "General Manager" and user_id in self.employees:
	        self.employees[user_id].position = position
	        self.dbo.update_employee(user_id,"position",position,"id")
	        return "Update Finished"
	    else:
	        return "Wrong parameters for update"
	        
	def update_name(self, user_id, name):
	    if user_id in self.employees:
	        self.employees[user_id].name = name
	        self.dbo.update_employee(user_id,"name",name,"id")
	        return "Update Finished"
	    else:
	        return "Wrong parameters for update"
	
	def update_manager(self, userid, superior):
	    if (
	        self.check_cycle(userid, superior) or  userid == self.general_manager 
	        or not userid in self.employees or not superior in self.employees
	       ):
	    	return "Cycle of General Manager changed"
	    else:
	        self.dbo.update_employee(userid, "manager_id", superior, "id")
	        older_manager = self.employees[userid].manager_id
	        self.employees[userid].manager_id = superior
	        self.managers[older_manager].remove(userid)
	        if superior in self.managers:
	            self.managers[superior].append(userid)
	            return "Update Finished"
	        else:
	            self.managers[superior]=[userid]
	            return "Update Finished"
	def remove_user_with_email(self, email):
	    users_with_email = dict((self.employees[item].email, item) for item in self.employees)
	    self.remove_user(users_with_email[email])
	    # TODO: write code...
	def remove_user(self, user_id):
	    if user_id == self.general_manager:
	    	return "Can not remove general_manager"
	    else:
	        if user_id in self.managers:
	            manager_id = self.employees[user_id].manager_id
	            for emp in self.managers[user_id]:
	                self.employees[emp].manager_id = manager_id
	                self.managers[manager_id].append(emp)
	            del self.managers[user_id]
	            self.managers[manager_id].remove(user_id)
	            del self.employees[user_id]
	            self.dbo.update_employee(user_id,"manager_id",manager_id,"manager_id")
	            self.dbo.delete_employee(user_id)
	        else:
	            manager_id = self.employees[user_id].manager_id
	            self.managers[manager_id].remove(user_id)
	            del self.employees[user_id]
	            self.dbo.delete_employee(user_id)
	            return "Remove Finished"
                
	def check_cycle(self, user_id, new_manager_id):
	    manager = self.employees[new_manager_id].manager_id
	    while not manager == 0 and not manager == user_id:
	        manager = self.employees[manager].manager_id
	    if manager == user_id:
		    return True
	    else:
	        return False

	def get_general_manager(self):
	    for emp in self.employees:
	        manager_id = self.employees[emp].manager_id
	        if manager_id == 0:
	            return emp
	
	def createView(self, user_id, level):
		if user_id in self.managers:
			for emp in self.managers[user_id]:
				self.hierarchy.append((str(self.employees[emp]),level+1))
				self.createView(emp, level + 1)
				
	def create_hierarchy(self):
	    self.hierarchy = [(str(self.employees[self.general_manager]),0)]
	    general_manager = self.general_manager
	    self.createView(general_manager, 0)
