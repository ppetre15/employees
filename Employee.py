class Employee():
	"""Employee class to retain Employee Name , Postion, ID and Manager ID"""
	def __init__(self, db_row):
		self.empid = db_row[0]
		self.name = db_row[1]
		self.position = db_row[2]
		self.manager_id = db_row[3]
		self.email = db_row[4]
	
	def __str__(self):
	  	return  self.name + "(" +self.position + ")" 