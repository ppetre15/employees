import MySQLdb
from Employee import Employee

class DBOperations():
	"""A class for working with the DB"""
	def __init__(self, host, user, password, dbname):
		self.db = MySQLdb.connect(host, user, password, dbname)
		self.cursor = self.db.cursor()
	
	def close(self):
		self.db.close()

	def get_all_employees(self):
		self.cursor.execute("SELECT * FROM EMPLOYEE");
		result = list(self.cursor.fetchall())
		all_employees = dict()
		for empl in result:
		    print empl
		    new_empl = Employee(empl)
		    all_employees[new_empl.empid] = new_empl
		return all_employees
	
	def get_employee(self, email):
	    self.cursor.execute("SELECT * FROM EMPLOYEE WHERE email = %S",email)
	    result = list(self.cursor.fetchone())
	    return result
    
    
	def create_employee(self, employee):
		try:
			self.cursor.execute(
					"""INSERT INTO EMPLOYEE (name, position, manager_id, email)
					VALUES (%s, %s, %s, %s)""",
					(employee.name, employee.position, employee.manager_id, employee.email),
				)
			self.db.commit()
		except Exception, e:
		    self.db.rollback()
		    raise e

	def delete_employee(self, employee_id):
		try:
			self.cursor.execute(
					"""DELETE FROM EMPLOYEE WHERE ID = %s""",
					[employee_id],
				)
			self.db.commit()
		except Exception, e:
		    self.db.rollback()
		    raise e

	def update_employee(self, employee_id, column_name, new_value, key_column_name):
		try:
			query = """UPDATE EMPLOYEE SET %s=%%s WHERE %s=%%s""" % (column_name,key_column_name)
			self.cursor.execute(
                    query,
					( new_value, employee_id),
				)
			self.db.commit()
		except Exception, e:
		    self.db.rollback()
		    raise e